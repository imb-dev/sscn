#
# Example 2x3- ANOVA:
# cellMeans <- matrix(c(rep(16, 3), 10, 12, 15), #c(rep(280, 3), 400, 350, 300), #
#                     ncol=3, byrow=TRUE,
#                     dimnames=list(A=c("CTR", "TRMT"), B=c("B1", "B2", "B3")))
# sigma_res <- 5
#
#
#
# ## an example: 2x2 design
# cellMeans <- matrix(c(8.5, 11, rep(7.5, 2)),
#                     ncol=2, byrow=TRUE,
#                     dimnames=list(A=c("CTRL pDC", "Treated pDC"), B=c("Ligand OFF", "Ligand ON")))
# sigma_res <- 1.75 #sqrt(3.1)
#
#
# # another example: 2x3 design
# cellMeans <- matrix(c(rep(8.5, 3), 11, 8.5, 8.5),
#                     ncol=3, byrow=TRUE,
#                     dimnames=list(A=c("low", "high"), B=c("CTRL", "TRT1", "TRT2")))
# sigma_res <- sqrt(3.1)
#
#
# # subset from precious example
# # cellMeans <- matrix(c(rep(8.5, 2), 11, 8.5),
# #                     ncol=2, byrow=TRUE,
# #                     dimnames=list(A=c("low", "high"), B=c("CTRL", "TRTA")))
#
#
#
# # example calculations
# # Factor A:
# #var.A <- stats::var(rowMeans(cellMeans))
# # adopt numerator
# var.A <- stats::var(rowMeans(cellMeans)) * (NROW(cellMeans)-1) / NROW(cellMeans)
#
# f.A <- sqrt(var.A) / sigma_res
# df.A <- NROW(cellMeans)-1
#
#
# # Factor B
# #var.B <- stats::var(colMeans(cellMeans))
# var.B <- stats::var(colMeans(cellMeans)) * (NCOL(cellMeans)-1) / NCOL(cellMeans)
# f.B <- sqrt(var.B) / sigma_res
# df.B <- NCOL(cellMeans)
#
#
# # Interaction AB: look at residuals after considering factor A and factor B
# # subtract first mean row means (factor A) and column means (factor B)
# cellResids <- as.vector(t(cellMeans - rowMeans(cellMeans)) - colMeans(cellMeans))
# #var.AB <- var(cellResids)
# var.AB <- var(cellResids) * (length(cellResids) - 1L) / length(cellResids)
#
# f.AB <- sqrt(var.AB) / sigma_res
# df.AB <- (NROW(cellMeans)-1L) * (NCOL(cellMeans)-1L)



# 1. Für den Haupteffekt `A` gilt: $f_A =$ `r f.A` mit Zählerfreiheitsgraden von `r df.A`.
# 2. Fur die Interaktion `AxB` gilt: $f_{AB} =$ `r f.AB` mit Zählerfreiheitsgraden von `r df.AB`.

# Reference with G*Power3: "F tests - ANOVA: Fixed effects, special, main effects and interactions"
# http://www.psycho.uni-duesseldorf.de/abteilungen/aap/gpower3/user-guide-by-distribution/f/anova_fixed_effects_special



#' Sample size calculation for a balanced multi-way ANOVA in a full-factorial design
#'
#' Given a balanced design for a multi-factorial ANOVA design with a given effect size in terms of expected values
#' the sample size per group is estimated to reach a requested power.
#'
#' @param design dataframe that provides the expected values under the alternative hypothesis
#' for each cell of the design in semi-long format (i.e. a column per factor; last column as expected mean response under HA)
#' @param sd numeric[1]. common residual standard deviation per cell
#' @param sig.level numeric[1]. the requested significance level.
#' @param power numeric[1]. named vector of length 1 with the power to be achieved for the effect that is specified by the name. If unnamed it is the power for the first main effect.
#' @return sscn object or `NULL` in case of errors on the way.
#' @export
ssc_mean_anova_n <- function(design, sd, sig.level=0.05, power) {
  stopifnot(is.numeric(sd), length(sd) == 1L, sd > 0L)
  stopifnot(is.numeric(power), length(power) == 1L, power < 1, power > 0)
  stopifnot(is.data.frame(design), NCOL(design) > 1, is.numeric(design[[NCOL(design)]]))
  # keep only complete cases in the design dataframe
  design <- design[stats::complete.cases(design),]

  nbrCells <- NROW(design) # nbr of cells (groups)
  # check we have at least two groups
  stopifnot(nbrCells > 1)
  # rename columns to standard names, last column is the response
  colnames(design) <- c(paste0("F_", colnames(design)[-NCOL(design)]), "mu")
  stopifnot(is.numeric(design[["mu"]]))

  fColsInd <- which(startsWith(colnames(design), "F_"))
  if (NROW(unique(design[, fColsInd, drop = FALSE])) != nbrCells) {
    warning("Please specify the expected values under the alternative hypothesis uniquely!",
            call. = FALSE)
    return(invisible(NULL))
  }

  # initialize corrected response (mu_c)
  design$mu_c0 <- design$mu_c <- design$mu - mean(design$mu)

  factorsLvls <- apply(design[,fColsInd, drop = FALSE], MARGIN = 2, FUN = function(x) length(unique(x)))
  stopifnot(length(factorsLvls) >= 1L)

  if (prod(factorsLvls) != nbrCells) {
    warning("Please specify the expected values under the alternative hypothesis for a **full**-factorial design!")
    return(invisible(NULL))
  }

  MAX_CHAR_LEN <- 10000L
  # all effects: main-effects, 2-way interaction effects, ...
  var_factors <- df1_factors <- purrr::set_names(numeric(2**length(factorsLvls) - 1))
  #set dummy names to have names() work
  i <- 0 # index for var_factors

  ## loop over types of interactions (main effects, 2-way interactions, 3-way interactions etc)
  for (j in seq_along(factorsLvls)) {
    ## list all unordered draws of size j from all factors without replacement
    facCombs <- utils::combn(x = names(factorsLvls), m = j)
    # loop over all concrete interactions per interaction type
    for (k in 1:NCOL(facCombs)) {
      facComb <- facCombs[, k]
      h <- design %>%
        dplyr::group_by(dplyr::across(dplyr::all_of(facComb))) %>%
        dplyr::summarise(mu_m = mean(mu_c), .groups = "drop")
      var_factors[[i+k]] <- stats::var(h$mu_m) * (NROW(h) - 1L) / NROW(h)
      df1_factors[[i+k]] <- prod(factorsLvls[facComb]-1L)

      # memory: use mean-aggregation to build up correction for current level j in column `resp_c0`
      design <- design %>%
        dplyr::inner_join(h, by = facComb) %>%
        dplyr::mutate(mu_c0 = mu_c0 - mu_m) %>%
        dplyr::select(-mu_m)
    } #rof k
    names(var_factors)[(i+1):(i+k)] <- names(df1_factors)[(i+1):(i+k)] <- apply(facCombs, 2, function(x) paste(substr(x, start = 3, stop = MAX_CHAR_LEN), collapse=":"))
    # update corrected response (after all interactions of order j are finished)
    stopifnot(isTRUE(all.equal(sum(design$mu_c0), 0L)))
    design$mu_c <- design$mu_c0
    i <- i + k
  } #rof j

  # calculate effect sizes for all involved (interaction) terms
  f_factors <- sqrt(var_factors) / sd
  eta_factors <- f_factors^2 / (1L + f_factors^2)
  stopifnot(length(df1_factors) == length(f_factors))

  powerfun_body <- quote({
    stats::pf(q = stats::qf(sig.level, df1 = df1_factors, df2 = N - nbrCells, lower.tail = FALSE),
       df1 = df1_factors, df2 = N - nbrCells, ncp = f_factors^2 * N, lower.tail = FALSE)  %>%
      purrr::set_names(names(f_factors)) # names for access later
  })

  pow_aimed_at <- names(power)
  if (is.null(pow_aimed_at)) {
    pow_aimed_at <- substr(names(factorsLvls)[1L], start = 3L, stop = MAX_CHAR_LEN)
    if (length(factorsLvls) > 1L) message("Calculating power for factor ", dQuote(pow_aimed_at))
  }
  stopifnot(length(pow_aimed_at) == 1, is.character(pow_aimed_at), pow_aimed_at %in% names(f_factors))
  targetFun_N <- function(N){ pown <- eval(powerfun_body); pown[[pow_aimed_at]] - power }
  ##targetFun_pow <- function(pow){ pow <- eval(powerfun_body); }

  n_raw <- NULL

  ## check if we have power at all
  if (is.na(targetFun_N(nbrCells+1))) {
    warning("Unkown term ", dQuote(pow_aimed_at), call. = FALSE)
  } else if (dplyr::near(f_factors[[pow_aimed_at]], 0)) {
    warning("The design does not seem to give power for term ", dQuote(pow_aimed_at), call. = FALSE)
  } else {
    try({
      n_raw <- stats::uniroot(targetFun_N, interval = c(nbrCells+.5, 3e+06), extendInt = "upX")$root / nbrCells
    }, silent = TRUE)
  }

  if (is.null(n_raw) || !is.finite(n_raw) || n_raw <= 0) {
    warning("Failed to find a solution for sample size n!", call. = FALSE)
    return(invisible(NULL))
  }

  n <- as.integer(ceiling(n_raw))
  N <- n * nbrCells
  # actual power for the selected n
  power <- eval(powerfun_body, envir = list(N=N))

  structure(
    list(id = "mean:anova", name = "ANOVA: mean comparisons in a balanced full-factorial design.",
         design = design %>%
           dplyr::select(!dplyr::all_of(c("mu_c", "mu_c0"))),
         f=f_factors, sd=sd, n_raw = n_raw, n=n, N=N,
         power = power),
    class = "sscn"
  )
}




#' Single-step Dunnett test sample size calculation
#'
#' Sample size calculation for Dunnett test for multiple comparison with a common control group. It is based on large sample asymptotics.
#'
#' @details
#' It supports one- and two-sided hypotheses and conjunctive (all-pairs) and disjunctive (any-pair/per-pair) power.
#' It does the calculation under the least favourable hypotheses configuration for the respective type of power.
#'
#' The function assumes equally sized treatment groups.
#' The control group can have same size as treatments group or be bigger with factor sqrt(k).
#' Variance is assumed to be constant in all groups and known.
#'
#' @param k number of experimental groups (not counting the control group)
#' @param biggerCtr flag if control group has equal size or if it is \eqn{\sqrt k} times the treatment groups. Defaults to \code{TRUE}.
#' @param delta effect size worth to be detected (vectorized)
#' @param sd known common standard deviation within the groups
#' @param sig.level significance level
#' @param power the requested power level
#' @param power.type which type of power. Choice between all-pairs (conjunctive) and any-pair (disjunctive) power
#' @param alternative one- or two-sided hypothesis
#' @return a list with the calculated minimal sample sizes
#' @references  Horn und Vollandt, 2000a
#' @export
ssc_mean_dunnett_n <- function(k, biggerCtr=TRUE, delta, sd, sig.level=0.05, power, power.type=c("all", "any"), alternative= c("two.sided", "one.sided")){

  alternative <- match.arg(alternative)
  power.type <- match.arg(power.type)

  NBR_TRTGROUPS <- 2:8
  QR <- c(0.50, 0.80, 0.90, 0.95)  # available quantile levels (1-alpha)

  R <- which(QR == 1-sig.level)
  C <- which( k == NBR_TRTGROUPS)

  stopifnot( is.numeric(k), k >= 2, k <= 8 )
  stopifnot( is.logical(biggerCtr) )
  stopifnot( length(R) == 1L )
  stopifnot( power.type != "all" || power %in% QR ) # for all-pairs power the quantiles are only available for the given levels

  Q_ROWNAMES <- paste(rep(QR, each=2), c("r=1", "r=sqrt(k)"), sep="_")


  # one-sided percentage points of the k-variate std. normal dist. u_{k, rho, 1-alpha} (cf Table 5 in Horn 2000a)
  Q1 <- matrix(c(
    0.39,0.42, 1.17,1.19, 1.58,1.59, 1.92,1.93,
    0.59,0.66, 1.34,1.38, 1.73,1.77, 2.06,2.09,
    0.72,0.83, 1.45,1.52, 1.84,1.89, 2.16,2.20,
    0.82,0.95, 1.54,1.62, 1.92,1.98, 2.23,2.28,
    0.89,1.05, 1.60,1.70, 1.98,2.06, 2.29,2.35,
    0.95,1.14, 1.65,1.77, 2.03,2.12, 2.34,2.41,
    1.00,1.21, 1.70,1.83, 2.07,2.17, 2.38,2.46),
    nrow=8, dimnames=list(Q_ROWNAMES, k=NBR_TRTGROUPS))


  # two-sided percentage points of the k-variate std. normal dist |u|_{k, rho, 1-alpha} (cf Table 6 in Horn 2000a)
  Q2a <- matrix(c(
    1.00,1.02, 1.58,1.59, 1.92,1.93, 2.21,2.22,
    1.18,1.22, 1.73,1.77, 2.06,2.09, 2.35,2.37,
    1.30,1.36, 1.84,1.89, 2.16,2.20, 2.44,2.47,
    1.38,1.46, 1.92,1.98, 2.23,2.28, 2.51,2.55,
    1.45,1.55, 1.98,2.05, 2.29,2.35, 2.57,2.61,
    1.51,1.62, 2.03,2.11, 2.34,2.41, 2.61,2.66,
    1.55,1.68, 2.07,2.17, 2.38,2.46, 2.65,2.71),
    nrow=8, dimnames=list(Q_ROWNAMES, k=NBR_TRTGROUPS))

  # percentage points u_{k, +/- rho, 1-alph} (cf Table 7 in Horn 2000a, but fixed 2nd entry)
  # for 2-sided all-pairs power calculations
  Q2b <- matrix(c(
    0.64,0.64, 1.28,1.28, 1.64,1.64, 1.96,1.96,
    0.84,0.84, 1.45,1.46, 1.80,1.81, 2.11,2.12,
    0.99,1.01, 1.58,1.60, 1.92,1.94, 2.21,2.23,
    1.09,1.12, 1.66,1.70, 2.00,2.02, 2.29,2.31,
    1.17,1.22, 1.73,1.78, 2.06,2.10, 2.35,2.38,
    1.24,1.30, 1.79,1.84, 2.11,2.16, 2.40,2.43,
    1.29,1.37, 1.84,1.90, 2.16,2.21, 2.44,2.48),
    nrow=8, dimnames=list(Q_ROWNAMES, k=NBR_TRTGROUPS))



  # looking up the correct quantiles q1 and q2 for calculation of lambda2
  q1 <- q2 <- 0L

  if ( power.type == 'any'){
    if ( alternative == "one.sided") {
      q1 <- Q1[1+(R-1)*2+biggerCtr, C]
      q2 <- qnorm(power)
    } else { # "two.sided"
      stopifnot( alternative == "two.sided")
      q1 <- Q2a[1+(R-1)*2+biggerCtr, C]
      q2 <- qnorm(power)
    }
  } else {
    stopifnot( power.type == 'all')
    q2R <- which(QR == power)
    stopifnot( length(q2R) == 1L )
    if ( alternative == "one.sided") {
      q1 <- Q1[1+(R-1)*2+biggerCtr, C]
      q2 <- Q1[1+(q2R-1)*2+biggerCtr, C]
    } else { # "two.sided"
      stopifnot( alternative == "two.sided")
      q1 <- Q2a[1+(R-1)*2+biggerCtr, C]
      q2 <- Q2b[1+(q2R-1)*2+biggerCtr, C]
    }
  }

  stopifnot( q1 > 0L, q2 > 0L )

  # ratio of sample sizes in control group vs treatment group
  r <- if (isTRUE(biggerCtr)) sqrt(k) else 1L

  lambda2 <- (1+1/r)*(q1 + q2)^2

  # From lambda2 there are easy formulas to get to n.
  # The following is for Dunnett's test. Horn (2000) give other factors for multiple U-Test and binomial test (after arcsin-transformation)
  n_raw <- lambda2 * sd^2/delta^2    # treatment group size
  n0_raw <- r * n_raw   # control group size

  # round
  n <- as.integer(ceiling(n_raw))  # treatment group size
  n0 <- as.integer(ceiling(n0_raw))  # control group size

  # enforce minimal group size
  n  <- max(N_MIN, n)
  n0 <- max(N_MIN, n0)

  # QQQ Horn is inconsistent if to round up *before* calculating n0.
  #+In Horn 1998 rounding is done at the very end which may lead to a smaller n0.
  ##return(list(n=ceiling(n), n0=ceiling(n0), total.N=n0+k*n)) ##Horn 1998
  structure(
    list(id = "mean:dunnett", name = "Dunnett: multiple mean comparisons to control group",
         delta=delta, sd=sd, n0_raw=n0_raw, n_raw=n_raw, n0=n0, n=n, N=n0+k*n,
         power = power, power.type = power.type),
    class = "sscn"
  )

}




#' Sample size calculation for Williams trend-test, by default a one-sided test for a monotonic alternative hypothesis.
#'
#' The function assumes equally sized treatment and control groups.
#' For the Williams test critical values we use a quick approximation due to Brown which seems to be a little conservative.
#' Variance is assumed constant and known.
#'
#' Only conjunctive (=all pairs) power calculation is available: Chow's calculation assumes that all dose-groups show the expected effect.
#'
#' @param k number of experimental groups (besides the control group)
#' @param sd known common standard deviation within the groups
#' @param delta effect size worth to be detected (vectorized)
#' @param sig.level significance level
#' @param power the requested power level
#' @param power.type which type of power. Choice between all-pairs (conjunctive) and any-pair (disjunctive) power
#' @param alternative one- or two-sided hypothesis
#' @param verbose numeric verbosity level. Default 0 is no extra output.
#' @return a list with the calculated minimal sample sizes
#' @references  Chow, sample size calculations 2008, Chap 11: dose response studies, p. 287
#' @export
ssc_mean_williams_n <- function(k, delta, sd, sig.level=0.05, power, power.type=c("all", "any"), alternative= c("one.sided", "two.sided"), verbose = 0){
  power.type <- match.arg(power.type)
  alternative <- match.arg(alternative)

  if (power.type != "all"){
    stop("Only all-pairs (conjunctive) power calculation available.")
  }

  if (alternative != "one.sided"){
    stop("Only one-sided power calculation available.")
  }

  ## iterative approach
  tol <- 1e-2
  n_prev <- 17L
  IT_MAX <- 301L


  #QQQ can end up in endless loop, in particular when there is huge std. effect delta / sd
  ### what about uniroot for expression "f - power~req~" where f is a function of n that calculates the resulting power
  i <- 1L
  repeat {
    # use Brown approximation of Williams critical values via simple t-quantiles
    n_new <-  2L * sd^2L * (qt(1L-sig.level/1.25, df = n_prev) + qnorm(power))^2 / delta^2

    if (i > IT_MAX / 3) n_new <- max(N_MIN, n_new)

    if (verbose >= 1) cat(sprintf("Iteration %d for n: %.4f  === > %.4f\n", i, n_prev, n_new))

    if (abs(n_new - n_prev) < tol || i > IT_MAX) break
    i <- i+1
    n_prev <- n_new
  }

  n <- max(N_MIN, ceiling(n_new))
  n0 <- n

  structure(
    list(id = "mean:williams", name = "Williams: monotonic mean trend test against a control group",
         delta=delta, sd=sd, n0=n0, n=n, N=n0+k*n,
         power = power, power.type = power.type,
         conv = if (i > IT_MAX) 1 else 0),
    class = "sscn"
  )
}





#' Simulation based power function for a controlled experiment (one-factor design) in a generalized linear model.
#'
#' The scenario is a one-factor experiment with control and different treatments.
#' Repeatedly, we simulate responses under the specified model with the specified effect.
#' By default, a Gaussian family is used (i.e. a regular ANOVA model setting)
#'
#' @param n0 size of control group
#' @param n size of the treatment groups. Either a single integer (equal sample size) or a integer vector of length k
#' @param effCoefs coefficients that represent difference in each group relative to the control on the linear predictor scale
#' @param fam specifies the GLM link-family. The default is the Gaussian family (e.g. a linear model)
#' @param estimFam logical. Should parameters of the family be estimated in the simulation? If FALSE, the given family parameter is directly used. (Relevant for e.g. negative binomial)
#' @param sd standard deviation of residual error (necessary for Gaussian family)
#' @param R number of bootstrap samples for simulation to evaluate power
#' @param power.type What type of power? Do we need to detect all hypotheses or is one significance of the tested hypotheses sufficient?
#' @param cmpType character[1]. This is forwarded to `glht`. Default is 'Dunnett'.
#' @param testAdjType character[1]. Forwarded to `glht`.
#' @return power-estimate
#' @export
ssc_mean_glm_sim_power <- function(n0, n, effCoefs = stop("Provide coefficients that reflect the effect under the alternative!"),
                                   fam = stats::gaussian(), sd = NULL, estimFam = TRUE,
                                   sig.level = 0.05, power.type = c("all", "any"),
                                   alternative = c("two.sided", "less", "greater"),
                                   cmpType = "Dunnett", testAdjType = "bonferroni",
                                   R = 501L, seed = NULL, verbose = 0) {

  cmpType <- match.arg(cmpType, choices = eval(formals(multcomp::contrMat)$type))
  testAdjType <- match.arg(testAdjType, choices = eval(formals(multcomp::adjusted)$type))
  power.type <- match.arg(power.type)
  alternative <- match.arg(alternative)

  stopifnot( is.numeric(effCoefs), length(effCoefs) >= 1L )
  k <- length(effCoefs)
  stopifnot( is.numeric(n0), is.numeric(n) )
  stopifnot( length(n0) == 1L, length(n) == 1L || length(n) == k )
  n0 <- trunc(n0);  n <- trunc(n)
  if (n0 < 1L || any(n < 1L)) stop("At least one of the groups has no observations.")
  if (length(n) == 1L && k > 1L) n <- rep(n, k)
  isGaussianFam <- fam$family == 'gaussian'

  stopifnot( isGaussianFam || is.function(fam$simulate) )



  if ( isGaussianFam && is.null(sd) ) stop("Provide standard deviation for residual error for Gaussian family!")

  if ( power.type == 'all' && cmpType %in% c("Marcus") )
    warning(glue::glue("Expecting all hypotheses of comparison type {dQuote(cmpType)} is not recommended!"))

  ## design
  des <- data.frame(fac = factor(x = rep(c("CTRL", paste0("TRTM", seq_len(k))),
                                         times = c(n0, n)))
  )
  ## fitted values (on response scale)
  mm <- stats::model.matrix(~fac, data = des,
                     contrasts = list(fac = "contr.treatment"))
  # set 0 arbitrarily as reference level for the control group
  des$`means~alt~` <- as.vector(fam$linkinv(mm %*% c(0, effCoefs)))

  if (verbose) {
    #dplyr::count(des, fac, `means~alt~`)
    designOverviewDF <- cbind(unique(des), n=c(n0, rep_len(n, k)))
    if (requireNamespace("knitr", quietly = TRUE)) {
      designOverviewDF <- knitr::kable(designOverviewDF)
    } #fi
    # caption is ignored for non-kable df-tables
    print(designOverviewDF,
          caption = "Effect under alternative, i.e. expected values per group on response scale, relative to 0 (ctrl group)")

  } #fi verbose

  # for reproducibility
  if (! is.null(seed)) {
    oRseed <- get(".Random.seed", envir = .GlobalEnv)
    set.seed(seed)
    on.exit(assign(".Random.seed", oRseed, envir = .GlobalEnv))
  }

  myResidDF <- NROW(des) - k - 1L
  # create minimal regression model object to use its simulate-functionality
  simDat <- stats::simulate(structure(list(fitted.values = des$`means~alt~`,
                                    family = fam,
                                    # for Gaussian family indirect way to specify SD
                                    df.residual = myResidDF,
                                    deviance = myResidDF * sd^2),
                               class = c("glm", "lm")),
                     nsim = R)


  fitGlmMod <- function(y, pAgg = base::min) {
    # QQQ Isn't stats::glm.fit faster than stats::glm?
    fmod <- if ( estimFam && grepl(pattern = "^Negative Binomial", x = fam$family, ignore.case = TRUE))
      suppressWarnings(MASS::glm.nb(y ~ fac, data = des)) else stats::glm(y ~ fac, family = fam, data = des)

    fmod_sum <- summary(multcomp::glht(fmod, linfct = multcomp::mcp(fac = cmpType), alternative = alternative),
                        test = multcomp::adjusted(type = testAdjType))

    pAgg(as.vector(fmod_sum$test$pvalues))
  }

  # parallel::mclapply does not work on windows!
  #pvalsList <- parallel::mclapply(X = simDat, FUN = fitGlmMod, mc.cores = mc.cores)
  # power.sim <- sum(sapply(pvalsList, FUN = function(x) if (power.type == 'all')
  #   all(x < sig.level) else any(x < sig.level) )) / R
  pvals <- future.apply::future_vapply(simDat, FUN.VALUE = double(1L),
                                       FUN = fitGlmMod, pAgg = c(min, max)[[1L + (power.type == 'all')]],
                                       future.seed = FALSE)

  pvals <- pvals[is.finite(pvals)]
  power.sim <- if (length(pvals) > 5L) (sum(pvals < sig.level)+1L) / (length(pvals) + 2L) else NA_real_


  structure(
    list(id = "mean:sim_glm",
         name = glue::glue("{cmpType}: multiple mean comparisons to control group in generalized linear model (GLM) with {fam$family}-family using R={R} simulations"),
         power.type = power.type, power=power.sim, sd=sd, n0=n0, n=n, N=sum(n0, n)),
    class = "sscn"
  )
}

