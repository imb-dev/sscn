# sscn 0.1.19.9000
* ANOVA: more robustness checks

# sscn 0.1.19
* Dunnett test: corrected an error in rounding of results (Andrea Gottschalk)

# sscn 0.1.18
* implement sample size calculation for Cochran-Armitage trend test

# sscn 0.1.17
* move delay-stuff to extra package `incubate`

# sscn 0.1.16
* bug fix: bind parameters works
* test statistic function inlined in bootstrap function (makes it future-proof)

# sscn 0.1.15
* Use Future-API for parallel computations in computation-heavy functions (e.g. simulation based power functions)
* improved plots for `test_delay_diff`

# sscn 0.1.14
* bug fix in bootstrap routine for delay estimation (bootstrapped data need to be sorted)

# sscn 0.1.13
* delay estimation: more check on minimal number of observations to be given

# sscn 0.1.12
* estimation of delay in parametric time-to-event analyses
* sample size estimation for 2-group difference in delay

# sscn 0.1.11
* Clean-up code
* use entry `N` to designate the _total_ sample size

# sscn 0.1.10
* `sscn_mean_anova_n` changes & improvements
    * outputs power for all factors involved in the ANOVA for the rounded-up `n` that is used. 
    * output slightly renamed
    * more argument checking

# sscn 0.1.9
* bug fix for ANOVA: degrees of freedom were wrong for interaction terms in designs bigger than 2x2 and a recycling bug in the `ncp=` argument of the call to `pf`
* added test for ANOVA from G*Power manual (that uses also observed power from SPSS' `GLM univariate`)

# sscn 0.1.8
* added sample size estimation for response surface design based on precision of response estimation
