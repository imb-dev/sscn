
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Package `sscn`

<!-- badges: start -->
<!-- badges: end -->

`sscn` provides functions around sample size calculation. This R-package
has gradually grown from different statistical counselling sessions
whenever sample size estimation was needed *and* whenever I invested the
extra time to dive deeper into the relevant publications *and* when I
thought it is desirable to implement the routine.

This might explain the *messy* nature of the package with ad-hoc
solutions and volatile code conventions. Nevertheless, I aim to cover
all functions with test code and to provide documentation and
references.

<matthias.kuhn@tu-dresden.de>

## Example

Calculate a necessary sample size for a one-way ANOVA in `sscn` like so:

``` r
library("sscn")

expectation <- data.frame(trtm = factor(c("CNTRL", "TRT A", "TRT B", "TRT C")),
                          mu = c(120, 110, 140, 150))

ssc_mean_anova_n(expectation, sd = 9, sig.level = 0.07, power = .9)
#> Sample size calculation for ANOVA: mean comparisons in a balanced full-factorial design.
#> $design
#>   F_trtm  mu
#> 1  CNTRL 120
#> 2  TRT A 110
#> 3  TRT B 140
#> 4  TRT C 150
#> 
#> $f
#>   trtm 
#> 1.7568 
#> 
#> $sd
#> [1] 9
#> 
#> $n_raw
#> [1] 2.2388
#> 
#> $n
#> [1] 3
#> 
#> $N
#> [1] 12
#> 
#> $power
#>    trtm 
#> 0.99202
```

The standard solution in R would be a call to `stats::power.anova.test`,
e.g.

``` r
power.anova.test(groups = 4, between.var = var(c(120, 110, 140, 150)), within.var = 9**2,
                 sig.level = .07, power = .9)
```

I hope that the specification of the expected effects in `sscn` is more
natural. What is more, the `sscn`-approach extends to higher-order
ANOVAs as well, — simply add columns for each additional factor and the
last column must have the expected mean values (e.g., column `mu`).

## sscn on more than one CPU-thread

<!-- R is a single-threaded program  -->

By default, `sscn` will use only a single thread of your CPU-cores. But
some functions in `sscn` which are rather computation heavy (e.g. the
simulation based power functions) can be instructed to use more than a
single thread. `sscn` makes use of the Future-API that is implemented by
the R-package `future`. So, to utilise e.g. 6 cores on your local
machine to estimate the all-pairs power for a multiple pairwise
comparison to a common control group in a linear model setting using
7000 simulation runs, do the following:

``` r
library("future")
plan(multisession, workers = 6)

library("sscn")
ssc_mean_glm_sim_power(n0 = 7, n = 4, k = 3,
      effCoefs = c(3, 1, 1.5, .85), sd = 1.2, power.type = "all", B = 7000)
```

You can choose from a number of Future-API implementations. Here is a
brief listing of important Future-backends:

|       Name        |          OSes           |                          Description                          |
|:-----------------:|:-----------------------:|:-------------------------------------------------------------:|
| **synchronous:**  |                         |                       **non-parallel:**                       |
|   `sequential`    |           all           |           sequentially and in the current R process           |
| **asynchronous:** |                         |                         **parallel:**                         |
|  `multisession`   |           all           |          background R sessions (on current machine)           |
|    `multicore`    | not Windows/not RStudio |            forked R processes (on current machine)            |
|     `cluster`     |           all           | external R sessions on current, local, and/or remote machines |
|     `remote`      |           all           |              Simple access to remote R sessions               |

Taken from the [introductory vignette of the
`future`-package](https://cran.r-project.org/web/packages/future/vignettes/future-1-overview.html),
section «Controlling How Futures are Resolved»

Call `future::plan(sequential)` at the end of your R-session to reset
any open connection or resource from your parallel/distributed
computations.

## Installation

You can install the `sscn` package from
[Gitlab](https://gitlab.com/imb-dev/sscn) with:

``` r
remotes::install_gitlab("imb-dev/sscn")
```

To install a specific version, add the version tag after the name,
separated by a `@`, e.g. to install `sscn` in version `v0.1.17` use

``` r
remotes::install_gitlab("imb-dev/sscn@v0.1.17")
```
