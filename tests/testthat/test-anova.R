# mkuhn, 2019-03-26
# Tests for the ANOVA


TOL_n <- 0.03
TOL_f <- 0.01

test_that("1-way ANOVA", code = {

  # Example from R's help page ?power.anova.test
  design_1way <- data.frame(
    trtm = gl(n = 4, k = 1, labels = LETTERS[1:4]),
    mu = c(120, 130, 140, 150))

  var_1way_within <- 500
  var_1way_betw <- var(design_1way$mu)
  myPows <- purrr::set_names(c(.5, .6, .72, .8, .9, .95, .99, .999),
                             ~ paste0("pow=", .))

  # compare results with answers from R's standard power function
  for (i in myPows) {
    expect_equal(sscn::ssc_mean_anova_n(design_1way,
                                        sd = sqrt(var_1way_within),
                                        sig.level = 0.15, power = i)$n_raw,
                 stats::power.anova.test(groups = NROW(design_1way), n=NULL,
                                  between.var = var_1way_betw, within.var = var_1way_within,
                                  sig.level = 0.15, power = i)$n,
                 tolerance = TOL_n)

    expect_equal(sscn::ssc_mean_anova_n(design_1way,
                                        sd = sqrt(var_1way_within),
                                        sig.level = 0.05, power = i)$n_raw,
                 power.anova.test(groups = NROW(design_1way), n=NULL,
                                  between.var = var_1way_betw, within.var = var_1way_within,
                                  sig.level = 0.05, power = i)$n,
                 tolerance = TOL_n)

    expect_equal(sscn::ssc_mean_anova_n(design_1way,
                                        sd = sqrt(var_1way_within),
                                        sig.level = 0.01, power = i)$n_raw,
                 power.anova.test(groups = NROW(design_1way), n=NULL,
                                  between.var = var_1way_betw, within.var = var_1way_within,
                                  sig.level = 0.01, power = i)$n,
                 tolerance = TOL_n)
  }

})

test_that("2-way ANOVA", {

  # example taken from an animal experiment on regeneration
  design_2way_a <- tibble::tribble(
    ~genotype, ~time, ~Y,
    "WT", "T1", 6,
    "WT", "T2", 12,
    "KO", "T1", 6,
    "KO", "T2", 9)

  # assume stronger interaction
  design_2way_b <- design_2way_a
  design_2way_b$Y[design_2way_b$genotype == 'KO' & design_2way_b$time == 'T2'] <- 8

  # expected numbers come from nQuery
  expect_identical(ssc_mean_anova_n(design_2way_a, sd = 2, sig.level = 0.05,
                                    power = c(`genotype:time`=.8))$n, 15L)
  expect_identical(ssc_mean_anova_n(design_2way_b, sd = 2, sig.level = 0.05,
                                    power = c(`genotype:time`=.8))$n, 9L)


  # from G*power manual that also shows «observed power» from SPSS `GLM univariate`
  #+(chap 11 F test: fixed effects ANOVA, p27f)
  design_3way_gpower <- expand.grid(C = 1:4,
                                    B = 1:3,
                                    A = 1:3,
                                    KEEP.OUT.ATTRS = FALSE) %>%
    # same order as expand_grid
    dplyr::select(A, B, C) %>%
    dplyr::mutate(mu = c(3, 14/3, 2, 2,  3, 2, 2, 2,  7/3, 2, 2, 2,
                         10/3, 10/3, 14/3, 14/3,  19/3, 14/3, 14/3, 14/3,  3, 14/3, 14/3, 14/3,
                         8/3, 8/3, 2, 2,  14/3, 2, 2, 2,  14/3, 2, 2, 2))


  # calculate eta from f
  # SPSS uses partial eta. This function can give both variants of eta.
  # @param adjust vector with N and k to adjust for SPSS-way to calculate eta or NULL
  # @return eta or partial eta
  f_to_eta <- function(f, adjust=NULL) {
    eta2 <- f^2 / (1+f^2)
    stopifnot(is.null(adjust) ||
                (length(adjust) == 2L && is.numeric(adjust) && all(c("N", "k") %in% names(adjust))))
    if (!is.null(adjust)) {
      eta2 <- eta2 * adjust[["N"]] / (adjust[["N"]] + adjust[["k"]] * (eta2-1))
    }
    eta2
  }
  # variance was given in the example
  sd_within <- sqrt(1.71296)

  adjSPSS <- c(
    k = NROW(design_3way_gpower), # nbr of groups
    N = NROW(design_3way_gpower)*3L  ## 3 replicates per group
  )



  # main effect A
  # n=3 gives a post-hoc power for main effect A of virtually 1
  aov_3w_A <- sscn::ssc_mean_anova_n(design_3way_gpower, sd = sd_within, sig.level = 0.05,
                                     power = c(A = .9999))
  # compare resulting f and n
  expect_equal(aov_3w_A$f[["A"]], expected = 0.7066856, tolerance = TOL_f)
  expect_equal(aov_3w_A$n, expected = 3)

  # main effect B
  expect_equal(sscn::ssc_mean_anova_n(design_3way_gpower, sd = sd_within, sig.level = 0.05,
                                      power = c(B = 0.152))$n_raw, 3L, tolerance = TOL_n)

  # main effect C
  aov_3w_C <- sscn::ssc_mean_anova_n(design_3way_gpower, sd = sd_within, sig.level = 0.05,
                                     power = c(C = 0.521))
  expect_equal(aov_3w_C$n_raw, expected = 3, tolerance = TOL_n)
  # transform effect size f to partial eta (like in SPSS)
  expect_equal(f_to_eta(aov_3w_C$f[["C"]], adjust = adjSPSS),
               expected = 0.081, tolerance = TOL_f)

  # 2-way interaction AB
  aov_3w_AB <- sscn::ssc_mean_anova_n(design_3way_gpower, sd = sd_within, sig.level = 0.05,
                                      power = c(`A:B` = .476))

  expect_equal(f_to_eta(aov_3w_AB$f[["A:B"]], adjust = adjSPSS),
               expected = 0.083, tolerance = TOL_f)
  expect_equal(aov_3w_AB$n_raw, expected = 3, tolerance = TOL_n)

  # 3-way interaction
  aov_3w_ABC <- sscn::ssc_mean_anova_n(design_3way_gpower, sd = sd_within, sig.level = 0.05,
                                       power = c(`A:B:C` = .513))

  expect_equal(f_to_eta(aov_3w_ABC$f[["A:B:C"]], adjust = adjSPSS),
               expected = 0.140, tolerance = TOL_f)
  expect_equal(aov_3w_ABC$n_raw, expected = 3, tolerance = TOL_n)


  # 2-way anova (case courtesy of FU)
  des2 <- tibble::tribble(
    ~A, ~B, ~Y,
    0, 0, 0.2,
    0, 1, 0.3,
    1, 0, 0.3,
    1, 1, 0.4,
    2, 0, 0.4,
    2, 1, 0.501,
  )
  #des2

  sscnObjA1 <- sscn::ssc_mean_anova_n(design = des2, sd = .1, power = c(A=.8), sig.level = 0.05)
  sscnObjA2 <- sscn::ssc_mean_anova_n(design = des2, sd = .1, power = c(A=.7), sig.level = 0.01)
  sscnObjB1 <- sscn::ssc_mean_anova_n(design = des2, sd = .1, power = c(B=.8), sig.level = 0.05)
  sscnObjB2 <- sscn::ssc_mean_anova_n(design = des2, sd = .1, power = c(B=.6), sig.level = 0.01)

  # expectations come from nQuery Advisor 7.0
  # nQuery will always keep the power at the requested level for the selected factor
  expect_identical(sscnObjA1$n, expected = 4L)
  expect_identical(floor(sscnObjA1$power * 100)[c("B", "A:B")],
                   expected = c(B=64, `A:B`=5))
  expect_identical(sscnObjA2$n, expected = 4L)
  expect_identical(floor(sscnObjA2$power * 100)[c("B", "A:B")],
                   expected = c(B=36, `A:B`=1))

  expect_identical(sscnObjB1$n, expected = 6L)
  expect_identical(floor(sscnObjB1$power * 100)[c("A", "A:B")],
                   expected = c(A=99, `A:B`=5))

  expect_identical(sscnObjB2$n, expected = 6L)
  expect_identical(floor(sscnObjB2$power * 100)[c("A", "A:B")],
                   expected = c(A=94, `A:B`=1))
})
